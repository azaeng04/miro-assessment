export const nameTxtField = () =>
  cy.get("[data-autotest-id=mr-form-signup-name-1]");

export const workEmailTxtField = () =>
  cy.get("[data-autotest-id=mr-form-signup-email-1]");

export const passwordTxtField = () =>
  cy.get("[data-autotest-id=mr-form-signup-password-1]");

export const signUpTermsChkBx = () => cy.get("#signup-terms");

export const signUpSubscribeChkBx = () => cy.get("#signup-subscribe");

export const signUpBtn = () =>
  cy.get("[data-autotest-id=mr-form-signup-btn-start-1]");

export const signUpModelTermsChkBx = () => cy.get("#tos-signup-terms");

export const signUpModelSubscribeChkBx = () => cy.get("#tos-signup-subscribe");

export const signUpWithGoogleBtn = () => cy.get("#kmq-google-button");

export const signUpWithSlackBtn = () => cy.get("#kmq-slack-button");

export const signUpWithOfficeBtn = () => cy.get("#kmq-office365-button");

export const signUpWithAppleBtn = () => cy.get("#apple-auth");

export const signUpWithFacebookBtn = () =>
  cy.get('img[title="Sign up with Facebook"]');

export const continueToSignUpBtn = () =>
  cy.get("[data-autotest-id=mr-form-gdpr-btn-signin-1]");

export const termsLnkTxt = () => cy.get("[data-autotest-id=mr-link-terms-2]");

export const privacyPolicyLnkTxt = () =>
  cy.get("[data-autotest-id=mr-link-privacy-1]");
