import {
  nameTxtField,
  passwordTxtField,
  workEmailTxtField,
  signUpTermsChkBx,
  signUpSubscribeChkBx,
  signUpBtn,
  signUpWithGoogleBtn,
  continueToSignUpBtn,
  signUpWithOfficeBtn,
  signUpWithFacebookBtn,
  termsLnkTxt,
  privacyPolicyLnkTxt,
} from "./sign-up.po";

import Chance from "chance";
const chance = new Chance();

describe("Sign Up", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("should display error message when no information is filled in on the form", () => {
    signUpBtn().click();

    cy.contains("div", "Please enter your password.").should("be.visible");
    cy.percySnapshot();
  });

  it("should display error message when no Name is provided", () => {
    workEmailTxtField().type("james@gmail.com");
    passwordTxtField().type("jamesjoans123");

    signUpTermsChkBx().check({ force: true });
    signUpSubscribeChkBx().check({ force: true });
    signUpBtn().click();

    cy.contains("div", "Please enter your name.").should("be.visible");
    cy.percySnapshot();
  });

  it("should display error message when no Work email is provided", () => {
    nameTxtField().type("James Joans");
    passwordTxtField().type("jamesjoans123");

    signUpTermsChkBx().check({ force: true });
    signUpSubscribeChkBx().check({ force: true });
    signUpBtn().click();

    cy.contains("div", "Please enter your email address.").should("be.visible");
    cy.percySnapshot();
  });

  it("should display error message when no Password is provided", () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("james@gmail.com");

    signUpTermsChkBx().check({ force: true });
    signUpSubscribeChkBx().check({ force: true });
    signUpBtn().click();

    cy.contains("div", "Please enter your password.").should("be.visible");
    cy.percySnapshot();
  });

  it("should display error message when the Terms are not agreed to", () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("james@gmail.com");
    passwordTxtField().type("jamesjoans123");

    signUpSubscribeChkBx().check({ force: true });
    signUpBtn().click();

    cy.contains("div", "Please agree with the Terms to sign up.").should(
      "be.visible"
    );
    cy.percySnapshot();
  });

  // Seems to fail due to application error
  it.skip("should move on to the email verification step when the Subscription checkbox is unchecked", () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type(chance.email());
    passwordTxtField().type("jamesjoans123");

    signUpTermsChkBx().check({ force: true });
    signUpBtn().click();

    cy.url().should("contain.text", "email-confirm");
  });

  it("should display error message the same work email address is used", () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("azaeng04@gmail.com");
    passwordTxtField().type("jamesjoans123");

    signUpTermsChkBx().check({ force: true });
    signUpBtn().click();

    cy.contains("div", "Sorry, this email is already registered").should(
      "be.visible"
    );
    cy.percySnapshot();
  });

  it("should display error message when password is less than 8 characters", () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("azaeng04@gmail.com");
    passwordTxtField().type("123");

    signUpTermsChkBx().check({ force: true });
    signUpBtn().click();

    cy.contains("div", "Please use 8+ characters for secure password").should(
      "be.visible"
    );
    cy.percySnapshot();
  });

  it('should display "So-so password" strength for 8 characters length', () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("james@gmail.com");
    passwordTxtField().type("123abc567");

    signUpTermsChkBx().check({ force: true });

    cy.contains("div", "So-so password").should("be.visible");
    cy.percySnapshot();
  });

  it('should display "Good password" strength for more than 8 characters length with numbers and special characters', () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("james@gmail.com");
    passwordTxtField().type("123abc567^&^");

    signUpTermsChkBx().check({ force: true });

    cy.contains("div", "Good password").should("be.visible");
    cy.percySnapshot();
  });

  it('should display "Great password" strength for more than 10 characters length with numbers and special characters', () => {
    nameTxtField().type("James Joans");
    workEmailTxtField().type("james@gmail.com");
    passwordTxtField().type("123abc567^&^@BC");

    signUpTermsChkBx().check({ force: true });

    cy.contains("div", "Great password").should("be.visible");
    cy.percySnapshot();
  });

  it("should display Terms pop up if the checkbox is not checked and any of the other Sign Up methods are selected", () => {
    signUpWithGoogleBtn().click();

    cy.get("body > div.socialtos.socialtos--visible > div").should(
      "be.visible"
    );
    cy.percySnapshot();
  });

  it("should display error message when Terms of Service is not checked when signing up with a Third Party", () => {
    signUpWithGoogleBtn().click();
    continueToSignUpBtn().click();

    cy.contains(
      "div",
      "Please agree with the Terms of Service and Privacy Policy"
    ).should("be.visible");
    cy.percySnapshot();
  });

  it('should navigate to Google sign in page when clicking "Sign Up with Google"', () => {
    signUpWithGoogleBtn().click();
    continueToSignUpBtn().then((value) => {
      const dataUrl = value.attr("data-url");
      expect(dataUrl).to.eql(
        "https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=1062019541050-8mvc17gv9c3ces694hq5k1h6uqio1cfn.apps.googleusercontent.com&redirect_uri=https://miro.com/social/google/&response_type=code&scope=profile%20email&include_granted_scopes=true"
      );
    });
  });
  it("should navigate to Microsoft sign in page when clicking the Office 365 icon", () => {
    signUpWithOfficeBtn().click();
    continueToSignUpBtn().then((value) => {
      const dataUrl = value.attr("data-url");
      expect(dataUrl).to.eql(
        "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=df49091e-274d-4e90-83b8-f4a003911de3&scope=openid+profile+email+https%3A//graph.microsoft.com/User.Read&response_type=code&redirect_uri=https%3A//miro.com/social/office365/"
      );
    });
  });

  it.skip("should navigate to Apple sign in page when clicking the Apple icon", () => {
    // There does not seem to be a link to assert against. Would need to find a different way to test this.
  });

  it("should navigate to Facebook sign in page when clicking the Facebook icon", () => {
    signUpWithFacebookBtn().click();
    continueToSignUpBtn().then((value) => {
      const dataUrl = value.attr("data-url");
      expect(dataUrl).to.eql(
        "https://graph.facebook.com/v2.12/oauth/authorize?client_id=210874652374013&redirect_uri=https://miro.com/social/facebook/&scope=email"
      );
    });
  });

  it("should navigate to Terms when Terms hyperlink is clicked", () => {
    termsLnkTxt().then((value) => {
      const url = value.attr("href");
      cy.visit(`https://miro.com${url}`);
    });
    cy.url().should("eql", "https://miro.com/legal/terms-of-service/");
    cy.contains("h2", "Terms of Service").should("be.visible");
    cy.percySnapshot();
  });

  it("should navigate to Privacy Policy when Privacy Policy hyperlink is clicked", () => {
    privacyPolicyLnkTxt().then((value) => {
      const url = value.attr("href");
      cy.visit(`https://miro.com${url}`);
    });
    cy.url().should("eql", "https://miro.com/legal/privacy-policy/");
    cy.contains("h2", "Privacy Policy").should("be.visible");
    cy.percySnapshot();
  });
});
