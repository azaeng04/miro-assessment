FROM cypress/included:7.2.0

RUN mkdir /app/
COPY package.json /app/
COPY yarn.lock /app/
COPY . /app/
WORKDIR /app/
RUN apt-get update && apt-get upgrade && apt-get install chromium -y
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true
ENV PUPPETEER_EXECUTABLE_PATH=/usr/bin/chromium
RUN yarn install

ENTRYPOINT []
